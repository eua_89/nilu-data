import os, sys, re
import getopt, errno  
import getpass    
import requests
import json
from requests.auth import HTTPBasicAuth
from pprint import pprint 




def get_upToDate(areas, stations, components):
    #resp = requests.get('https://api.nilu.no/aq/utd.json?areass=Trondheim&stationss=Torvet&componentss=NO2')
    url = 'https://api.nilu.no/aq/utd.json?areas='+str(areas)+'&stations='+str(stations)+'&components='+str(components)
    print url
    resp = requests.get(url)
    if resp.status_code != 200:
    # This means something went wrong.
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))
    
    data = resp.json()
    return data

def get_historical(areas, stations, components, from_time, to_time):
    #https://api.nilu.no/aq/historical/NO2.json?fromtime=2017.01.02%2009:00&totime=2017.01.04%2015:00&areas=Trondheim&stations=Torvet
    url = 'https://api.nilu.no/aq/historical/NO2.json?fromtime='+str(from_time)+'&totime='+str(to_time)+'&areas='+str(areas)+'&stations='+str(stations)
    print url
    resp = requests.get(url)
    if resp.status_code != 200:
    # This means something went wrong.
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))
    
    data = resp.json()
    return data



def test_get_upToDate():
    
    #DEFINE which area/city you want
    areas = ('Trondheim','Oslo')
    if len(areas)>1:
        areas = ';'.join((areas))
    else:
        areas = areas[0]
    
    #DEFINE which station is the city you want
    stations = ('Torvet','Bakke kirke')
    if len(stations)>1:
        stations = ';'.join((stations))
    else:
        stations= stations[0]
    
    #DEFINE which measurement/component you want to get eg NO2, NO... (if requested historical data it is only allowed to chose one component)
    components = ('NO2',)
#    print(len(components))
    if len(components)>1:
        components = ';'.join((components))
    else: 
        components = components[0]
    
    #DEFINE start date that you want your historical data to start from. 
    from_time = '2017.01.02%2009:00'    
#    print(len(from_time))
    
    #DEFINE stop date that you want your historical data to stop to (maximum 6 months per request is allowed).
    to_time = '2017.01.04%2015:00'
#    print(len(to_time))

    if len(from_time)==0 and len(to_time)==0:
        print get_upToDate(areas, stations, components)
    elif (len(from_time)==0 and len(to_time)>0) or (len(from_time)>0 and len(to_time)==0):
        print('You need to define both from and to time')
#    elif (len(from_time)>0 and len(to_time)>0 and len(components)>=2):
#        print('You can only get info of one component when you request historical data (defining from_time, to_time)')
    else:
        print get_historical(areas, stations, components, from_time, to_time)
        


def print_choices():
    url='https://api.nilu.no/aq/utd.json'
    resp = requests.get(url)
    if resp.status_code != 200:
    # This means something went wrong.
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))
    
    data = resp.json()
#    print(data)
    
    list_areas = []
    for i in data:
        list_areas.append(i['area'])
        list_areas = list(set(list_areas))
    print('This is a list with all the available areas provided by NILU:\n')
    for p in list_areas: print p
    
    #DEFINE the area you are interested in
    area = 'Oslo'
    stations = (filter(lambda set:set['area']==area,data))
    print('This is a list with all the available NILU stations in the area: '+area+'\n')
    for i in stations:
        print i['station']

    
    
    k = raw_input ('Press enter to continue otherwise press any other key')
    if k== '':
        test_get_upToDate()
    else:
        print ('Bye')



if __name__ == '__main__':
    print_choices()