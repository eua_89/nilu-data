# README #

### Nilu Rest Api ###

* Official documentation about the NILU Rest API can be found in  [this](https://api.nilu.no/docs/) link. 
* Area(s), Station(s), Component(s) of interest can be defined for real time data. 
In case you define only one of the above you should write it in a parenthesis whith a comma after your option e.g. components = ('NO2',)
* Area(s), Station(s), Component, Start Time, Stop Time of interest can be defined for historical data. 
You can only get info of one component when you request historical data

